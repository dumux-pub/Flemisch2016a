namespace Dumux{
template<typename Grid>
class globalBoundary{
public:
    typedef typename Grid::LeafGridView GV;
    typedef typename GV::template Codim<0>::Iterator GridIterator;
    typedef typename GV::IntersectionIterator IntersectionIterator;
    typedef typename Grid::Traits::GlobalIdSet::IdType GridIdType;
    typedef typename Grid::ctype ctype;
    static const unsigned int dimworld_=Grid::dimensionworld;
    typedef Dune::FieldVector<ctype,dimworld_> FV;
    typedef typename std::vector<FV> VectorOfFVs;
    typedef typename VectorOfFVs::iterator VectorOfFVsIt;
    typedef typename VectorOfFVs::const_iterator VectorOfFVsCIt;

    globalBoundary(const Grid &grid){
        assert(dimworld_==2);
        typedef typename std::vector<VectorOfFVs> BoundarySegmentsUnsorted ;
        BoundarySegmentsUnsorted boundarySegmentsUnsorted;

        GV gv = grid.leafGridView();
        GridIterator eEndIt = grid.template leafend<0>();
        for (GridIterator eIt = grid.template leafbegin<0>(); eIt != eEndIt; ++eIt) {
//            GridIdType eId = grid.globalIdSet().id(*eIt);//global matrix element id
            IntersectionIterator isItEnd = gv.iend(*eIt);
            for (IntersectionIterator isIt = gv.ibegin(*eIt); isIt != isItEnd; ++isIt){
                if(isIt->boundary()){
                    VectorOfFVs boundarySegment;
                    boundarySegment.push_back(isIt->geometry().corner(0));
                    boundarySegment.push_back(isIt->geometry().corner(1));
//                    std::cout << "boundary segment: " << isIt->geometry().corner(0) <<"\t" << isIt->geometry().corner(1) << std::endl;
                    assert(boundarySegment.size()==2);
                    boundarySegmentsUnsorted.push_back(boundarySegment);
                }
            }
        }

        /*
         * Sort the boundary nodes by comparing two boundarySegments and
         * finding the ones wich share the same node
         */

        boundaryNodesSorted_.push_back( boundarySegmentsUnsorted.back()[0] );
        boundarySegmentsUnsorted.pop_back();
        while (boundarySegmentsUnsorted.size()!=0){
            typename BoundarySegmentsUnsorted::iterator boundarySegmentsUnsortedIterator;
            for (boundarySegmentsUnsortedIterator=boundarySegmentsUnsorted.begin();
                    boundarySegmentsUnsortedIterator!=boundarySegmentsUnsorted.end();
                    ++boundarySegmentsUnsortedIterator){
                if (Dumux::arePointsEqual(boundaryNodesSorted_.back(),(*boundarySegmentsUnsortedIterator)[0]) ) {
                    boundaryNodesSorted_.push_back( (*boundarySegmentsUnsortedIterator)[1] );
                    boundarySegmentsUnsorted.erase(boundarySegmentsUnsortedIterator);
                    break;
                }
                else if (Dumux::arePointsEqual(boundaryNodesSorted_.back(),(*boundarySegmentsUnsortedIterator)[1]) ) {
                    boundaryNodesSorted_.push_back( (*boundarySegmentsUnsortedIterator)[0] );
                    boundarySegmentsUnsorted.erase(boundarySegmentsUnsortedIterator);
                    break;
                }
            }//end for
        }//end while

        /*
         * pick one boundary node
         * calculate the vectors between this node and both adjacent boundary nodes
         * if the vectors are parallel, i.e., abs(scalar product)==1, delete this node
         */
        for (VectorOfFVsIt boundaryNodesSortedIt=boundaryNodesSorted_.begin();
                boundaryNodesSortedIt!=boundaryNodesSorted_.end();
                ++boundaryNodesSortedIt){
            FV vector1(*boundaryNodesSortedIt);
            FV vector2(*boundaryNodesSortedIt);
            /*
             * Go to the next element.
             * If the recent element is the last, the next is the first in the list.
             */
            ++boundaryNodesSortedIt;
            if (boundaryNodesSortedIt == boundaryNodesSorted_.end()) {boundaryNodesSortedIt=boundaryNodesSorted_.begin();}

            vector1-=(*boundaryNodesSortedIt);
            vector1/=vector1.two_norm();
            if (boundaryNodesSortedIt == boundaryNodesSorted_.begin()) {boundaryNodesSortedIt=boundaryNodesSorted_.end();}
            --boundaryNodesSortedIt;
            if (boundaryNodesSortedIt == boundaryNodesSorted_.begin()) {boundaryNodesSortedIt=boundaryNodesSorted_.end();}
            --boundaryNodesSortedIt;
            vector2-=(*boundaryNodesSortedIt);
            vector2/=vector2.two_norm();
            ++boundaryNodesSortedIt;
            if (boundaryNodesSortedIt == boundaryNodesSorted_.end()) {boundaryNodesSortedIt=boundaryNodesSorted_.begin();}
            if ( Dumux::arePointsEqual(std::abs(vector1*vector2),1.0) ){
                boundaryNodesSorted_.erase(boundaryNodesSortedIt);
                --boundaryNodesSortedIt;
            }
        }

        //debug output
//        for (VectorOfFVsCIt boundaryNodesSortedIt=boundaryNodesSorted_.begin();
//                        boundaryNodesSortedIt!=boundaryNodesSorted_.end();
//                        ++boundaryNodesSortedIt){
//            std::cout << "boundary node: " << *boundaryNodesSortedIt << std::endl;
//        }
    }//end constructor

    /*
     * position is a global position
     */
    template<typename FV>
    bool onGlobalBoundary(const FV &position) const {
        for (VectorOfFVsCIt boundaryNodesSortedIt=boundaryNodesSorted_.begin();
                boundaryNodesSortedIt!=boundaryNodesSorted_.end();
                ++boundaryNodesSortedIt){
            const FV thisPoint=*boundaryNodesSortedIt;

            ++boundaryNodesSortedIt;
            if (boundaryNodesSortedIt == boundaryNodesSorted_.end()) {boundaryNodesSortedIt=boundaryNodesSorted_.begin();}
            const FV nextPoint=*boundaryNodesSortedIt;
            if (boundaryNodesSortedIt == boundaryNodesSorted_.begin()) {boundaryNodesSortedIt=boundaryNodesSorted_.end();}
            --boundaryNodesSortedIt;
            if (Dumux::pointOnLine(thisPoint, nextPoint, position)) {
//                std::cout << "thisPoint: " << thisPoint << "\t next: " << nextPoint << "\t position: " << position <<std::endl;
                return (true);
            }
        }
        return (false);
    }//end onGlobalBoundary

private:
    VectorOfFVs boundaryNodesSorted_;
};
}//end namespace Dumux
