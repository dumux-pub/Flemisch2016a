#ifndef FRACTURESOLUTION_HH_
#define FRACTURESOLUTION_HH_
#include "../common/matrixsolution.hh"
namespace Dumux{

template<typename FractureGrid, typename FGFS, typename X, typename MatrixSolution>
class FractureSolution{

    typedef typename FractureGrid::LeafGridView GV;
    typedef typename FractureGrid::ctype ctype;
    typedef typename GV::template Codim<0>::Iterator FractureGridIterator;
    typedef typename Dune::PDELab::LocalFunctionSpace<FGFS> LFSU;

    typedef typename LFSU::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::DomainType FVLocal;
    typedef typename LFSU::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeFieldType Scalar;
    typedef typename LFSU::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::RangeType R;
    typedef typename LFSU::Traits::SizeType sizeType;
    typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;

    typedef Dune::PDELab::LFSIndexCache<LFSU> FLFSCache;
    typedef typename X::template LocalView<FLFSCache> XView;
    typedef typename X::template ConstLocalView<FLFSCache> ConstXView;

    //typedefs for global evaluation of pressure via grid function
    typedef Dune::PDELab::DiscreteGridFunction<FGFS,X> FDGF;
    typedef Dune::PDELab::GridFunctionToFunctionAdapter<FDGF> FGF;
    typedef typename FGF::Traits::RangeType RangeTypeGF;

public:
FractureSolution(const FractureGrid& fractureGrid, const FGFS& fgfs,const X& x, const MatrixSolution &matrixSolution)
    : fractureGrid_(fractureGrid), fgfs_(fgfs), x_(x), matrixSolution_(matrixSolution),
      lfsu_(fgfs),
      fdgf_(fgfs,x), fgf_(fdgf_)
{}

template<typename FV>
RangeTypeGF evaluatePressureGlobal(const FV &global) {
//    std::cout << "global: " << global << std::endl;
    RangeTypeGF pF;
    fgf_.evaluate(global,pF);
//    std::cout << "pressure: " << pF << std::endl;
    return (pF);
}//end evaluate function



template<typename Soil, typename VV>
void evaluateVelocity(const Soil &soil, VV &velocitiesVector) {

    typedef Dune::FieldVector<ctype,dimw_> FVGlobal;
    typedef Dune::FieldVector<R,dim_> GradPhi;


    FLFSCache multilfs_cache(lfsu_);
    ConstXView x_view(x_);

FractureGridIterator eendit = fractureGrid_.template leafend<0>();
for (FractureGridIterator elementIterator = fractureGrid_.template leafbegin<0>(); elementIterator != eendit; ++elementIterator) {
    typedef typename Dune::FieldVector<Scalar,dim_> GradP;

        FLFSCache lfs_cache_(lfsu_);
        ConstXView x_view_(x_);

        R u_fe(0.0);//TODO check what happens if u_fe is returned without a proper value (u_fe=0.0)?

        lfsu_.bind(*elementIterator);

        std::vector<R> xl(lfsu_.size());        // local coefficients
        lfs_cache_.update();
        x_view_.bind(lfs_cache_);
        x_view_.read(xl);// get local coefficients of the solution

        FVGlobal globalElementCenter = elementIterator->geometry().center();
        FVLocal localElementCenter=elementIterator->geometry().local(globalElementCenter);

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js(lfsu_.size());
        lfsu_.finiteElement().localBasis().evaluateJacobian(localElementCenter,js);

        // transform gradients from reference element to real element, but in fracture coordinates (i.e. adjusting only the length)
        ctype jac= elementIterator->geometry().integrationElement(localElementCenter);//TODO integrationElement is constant at every integration point. so it could be placed outside this loop
        GradPhi gradphif(lfsu_.size());//lfsu.size()=2 for 1D line elements here
        for (sizeType i=0; i<lfsu_.size(); i++)
        {
            gradphif[i]=js[i][0]/jac;//TODO: Only 1D, re-think for planes in 3D environment
        //            std::cout << "gradphiF: " << gradphif[i] << std::endl;
        }

        // compute gradient of pf
        GradP gradP(0.0);
        for (sizeType i=0; i<lfsu_.size(); i++){
            gradP.axpy(xl[lfsu_.localIndex(i)],gradphif[i]);
        }
        const Scalar KFT=soil.kFT(*elementIterator);
        const Scalar KFN=soil.kFN(*elementIterator);
        const Scalar aperture=soil.fractureWidth(*elementIterator);
        typename VV::value_type u(0.0);
        u[0]=-1.0*KFT*gradP*aperture;//tangential part of the velocity in the fracture


        /*
         * normal part of the velocity with regards to matrix domain
         * is calculated in normal direction, i.e., if the pressure in the fracture
         * is higher than the pressure in the matrix on that side, the velocity will be positive.
         *
         * If for example both normal velocities are positive there is only flow from the fracture into
         * both sides of the matrix.
         *
         */

        // evaluate basis functions on reference element
        std::vector<R> phif(lfsu_.size());
        lfsu_.finiteElement().localBasis().evaluateFunction(localElementCenter,phif);

        // compute pf at integration point
        Scalar pf=0.0;
        for (sizeType i=0; i<lfsu_.size(); i++){
            pf +=xl[lfsu_.localIndex(i)]*phif[i];
        }

        /*
         * this calculation is very sensitive to the Dumux::equalityEpsilon value!
         */
       const  Scalar gradPN1= (matrixSolution_.evaluatePPlus(globalElementCenter)-pf)/(soil.fractureWidth(*elementIterator)/2.0);//matrixSolution_.dfn()
//       std::cout << std::scientific << std::setprecision(18) << "p1: " << matrixSolution_.evaluatePPlus(globalElementCenter) << "\t pf: " << pf << "\t \t gradp: " << gradPN1 << std::endl;
        u[1]=-1.0*KFN*gradPN1;//normal part of the velocity with regards to matrix domain one
       const  Scalar gradPN2= (matrixSolution_.evaluatePMinus(globalElementCenter)-pf)/(soil.fractureWidth(*elementIterator)/2.0);// matrixSolution_.dfn()
        u[2]=-1.0*KFN*gradPN2;//normal part of the velocity with regards to matrix domain two


        velocitiesVector.push_back(u);
        std::cout << "uf("<<globalElementCenter<<")= " << u << std::endl;
}//end loop over all elements

}//end evaluate velocities function

private:
    const FractureGrid &fractureGrid_;
    const FGFS &fgfs_;
    const X &x_;
    const MatrixSolution &matrixSolution_;
    LFSU lfsu_;

    FDGF fdgf_;
    FGF fgf_;

    static const int dimw_=FractureGrid::dimensionworld;
    static const int dim_=FractureGrid::dimension;

};//end class FractureSolution
}//end namespace Dumux
#endif /* FRACTURESOLUTION_HH_ */
