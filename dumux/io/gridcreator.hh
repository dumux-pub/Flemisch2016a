// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Provides a grid creator for all supported grid managers with
 *        input file interfaces.
 *
 * \todo add Petrel grids with dune-cornerpoint
 */
#ifndef DUMUX_GRID_CREATOR_HH
#define DUMUX_GRID_CREATOR_HH

#include <array>
#include <bitset>
#include <memory>
#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/common/classname.hh>
#include <dune/common/parallel/collectivecommunication.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/io/file/dgfparser/dgfparser.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

// YaspGrid specific includes
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>

 // OneDGrid specific includes
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfoned.hh>

// UGGrid specific includes
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#endif

// ALUGrid specific includes
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif

// FoamGrid specific includes
#if HAVE_DUNE_FOAMGRID
#include <dune/foamgrid/foamgrid.hh>
#include <dune/foamgrid/dgffoam.cc>
#endif

#include <dumux/common/propertysystem.hh>
#include <dumux/common/parameters.hh>

namespace Dumux
{
namespace Properties
{
// poperty forward declarations
NEW_PROP_TAG(Grid);
NEW_PROP_TAG(GridCreator);
NEW_PROP_TAG(AdaptiveGrid);
NEW_PROP_TAG(Scalar);
NEW_PROP_TAG(ImplicitIsBox);
}


/*!
 * \brief Provides the grid creator base interface (public) and methods common
 *        to most grid creator specializations (protected).
 */
template <class TypeTag, class Grid>
class GridCreatorBase
{
    using Implementation = typename GET_PROP_TYPE(TypeTag, GridCreator);
    using Intersection = typename Grid::LeafIntersection;
    using Element = typename Grid::template Codim<0>::Entity;
public:

    /*!
     * \brief Returns a reference to the grid.
     */
    static Grid &grid()
    {
        if(enableDgfGridPointer_)
            return *dgfGridPtr();
        else
            return *gridPtr();
    }

    /*!
     * \brief Call the parameters function of the DGF grid pointer if available
     */
    template <class Entity>
    static const std::vector<double>& parameters(const Entity& entity)
    {
        if(enableDgfGridPointer_)
            return dgfGridPtr().parameters(entity);
        else
            DUNE_THROW(Dune::InvalidStateException, "The parameters method is only available if the grid was constructed with a DGF file!");
    }

    /*!
     * \brief Call the parameters function of the DGF grid pointer if available
     */
    template <class GridImp, class IntersectionImp>
    static const Dune::DGFBoundaryParameter::type& parameters(const Dune::Intersection<GridImp, IntersectionImp>& intersection)
    {
        if(enableDgfGridPointer_)
            return dgfGridPtr().parameters(intersection);
        else
            DUNE_THROW(Dune::InvalidStateException, "The parameters method is only available if the grid was constructed with a DGF file!");
    }

    /*!
     * \brief Return the boundary domain marker (Gmsh physical entity number) of an intersection
              Only available when using Gmsh with GridParameterGroup.DomainMarkers = 1.
     * \param boundarySegmentIndex The boundary segment index of the intersection (intersection.boundarySegmentIndex()
     */
    static const int getBoundaryDomainMarker(int boundarySegmentIndex)
    {
        if(enableGmshDomainMarkers_)
        {
            if (boundarySegmentIndex >= grid().numBoundarySegments())
                DUNE_THROW(Dune::RangeError, "Boundary segment index "<< boundarySegmentIndex << " bigger than number of bonudary segments in grid!");
            return boundaryMarkers_[boundarySegmentIndex];
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "The getBoundaryDomainMarker method is only available if DomainMarkers for Gmsh were enabled!"
                                                     << " If your Gmsh file contains domain markers / physical entities,"
                                                     << " enable them by setting " //<< GET_PROP_VALUE(TypeTag, GridParameterGroup)
                                                     << "Grid.DomainMarkers = 1 in the input file.");
    }

    /*!
     * \brief Return the element domain marker (Gmsh physical entity number) of an element.
              Only available when using Gmsh with GridParameterGroup.DomainMarkers = 1.
     * \param elementIdx The element index
     */
    DUNE_DEPRECATED_MSG("This will often produce wrong parameters in case the grid implementation resorts the elements after insertion. Use getElementDomainMarker(element) instead!")
    static const int getElementDomainMarker(int elementIdx)
    {
        if(enableGmshDomainMarkers_)
        {
            if(elementIdx >= grid().levelGridView(0).size(0))
                DUNE_THROW(Dune::RangeError, "Requested element index is bigger than the number of level 0 elements!");
            return elementMarkers_[elementIdx];
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "The getElementDomainMarker method is only available if DomainMarkers for Gmsh were enabled!"
                                                     << " If your Gmsh file contains domain markers / physical entities,"
                                                     << " enable them by setting " //<< GET_PROP_VALUE(TypeTag, GridParameterGroup)
                                                     << "Grid.DomainMarkers = 1 in the input file.");
    }

    static const int getVertexMarker(int vIdx)
    {
        return vertexMarkers_[vIdx];
    }

    /*!
     * \brief Return the element domain marker (Gmsh physical entity number) of an element.
              Only available when using Gmsh with GridParameterGroup.DomainMarkers = 1.
     * \param elementIdx The element index
     */
    static const int getElementDomainMarker(const Element& element)
    {
        if(enableGmshDomainMarkers_)
        {
            auto insertionIndex = gridFactory().insertionIndex(element);
            if(insertionIndex >= grid().levelGridView(0).size(0))
                DUNE_THROW(Dune::RangeError, "Requested element index "<< insertionIndex <<" is bigger than the number of level 0 elements!");
            return elementMarkers_[insertionIndex];
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "The getElementDomainMarker method is only available if DomainMarkers for Gmsh were enabled!"
                                                     << " If your Gmsh file contains domain markers / physical entities,"
                                                     << " enable them by setting " //<< GET_PROP_VALUE(TypeTag, GridParameterGroup)
                                                     << "Grid.DomainMarkers = 1 in the input file.");
    }

    /*!
     * \brief Call loadBalance() function of the grid.
     */
    static void loadBalance()
    {
        if(enableDgfGridPointer_)
            dgfGridPtr().loadBalance();
        else
            gridPtr()->loadBalance();
    }

protected:

    /*!
     * \brief Returns a reference to the grid pointer (std::shared_ptr<Grid>)
     */
    static std::shared_ptr<Grid> &gridPtr()
    {
        if(!enableDgfGridPointer_)
        {
            static std::shared_ptr<Grid> gridPtr_;
            return gridPtr_;
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "You are using DGF. To get the grid pointer use method dgfGridPtr()!");
    }

    /*!
     * \brief Returns a reference to the grid factory
     */
    static Dune::GridFactory<Grid> &gridFactory()
    {
        static Dune::GridFactory<Grid> gridFactory_;
        return gridFactory_;
    }

    /*!
     * \brief Returns a reference to the DGF grid pointer (Dune::GridPtr<Grid>).
     */
    static Dune::GridPtr<Grid> &dgfGridPtr()
    {
        if(enableDgfGridPointer_)
        {
            static Dune::GridPtr<Grid> dgfGridPtr_;
            return dgfGridPtr_;
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "The DGF grid pointer is only available if the grid was constructed with a DGF file!");
    }

    /*!
     * \brief Returns the filename extension of a given filename
     */
    static std::string getFileExtension(const std::string& fileName)
    {
        std::size_t i = fileName.rfind('.', fileName.length());
        if (i != std::string::npos)
        {
            return(fileName.substr(i+1, fileName.length() - i));
        }
        else
        {
            DUNE_THROW(Dune::IOError, "Please provide and extension for your grid file ('"<< fileName << "')!");
        }
        return "";
    }

    /*!
     * \brief Makes a grid from a file. We currently support *.dgf (Dune Grid Format) and *.msh (Gmsh mesh format).
     */
    static void makeGridFromFile(const std::string& fileName)
    {
        // We found a file in the input file...does it have a supported extension?
        const std::string extension = getFileExtension(fileName);
        if(extension != "dgf" && extension != "msh")
            DUNE_THROW(Dune::IOError, "Grid type " << Dune::className<Grid>() << " only supports DGF (*.dgf) and Gmsh (*.msh) grid files but the specified filename has extension: *."<< extension);

        // make the grid
        if(extension == "dgf")
        {
            enableDgfGridPointer_ = true;
            dgfGridPtr() = Dune::GridPtr<Grid>(fileName.c_str(), Dune::MPIHelper::getCommunicator());
        }
        if(extension == "msh")
        {
            // get some optional parameters
            bool verbose = false;
            try { verbose = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Grid, Verbosity);}
            catch (Dumux::ParameterException &e) { }

            bool boundarySegments = false;
            try { boundarySegments = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Grid, BoundarySegments);}
            catch (Dumux::ParameterException &e) { }

            bool domainMarkers = false;
            try { domainMarkers = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Grid, DomainMarkers);}
            catch (Dumux::ParameterException &e) { }

            if (domainMarkers)
                enableGmshDomainMarkers_ = true;

            // leave this to the implementation as some grids need special treatment here
            // e.g. ALUGrid needs the grid to be read only on rank 0 while ug
            Implementation::makeGridFromGmshFile(fileName, verbose, boundarySegments, domainMarkers);
        }
    }

    /*!
     * \brief Creates a gmsh grid from file
     */
    static void makeGridFromGmshFile(const std::string& fileName, bool verbose, bool boundarySegments, bool domainMarkers)
    {
        // as default read it on all processes in parallel
        if(domainMarkers)
        {
            Dune::GmshReader<Grid>::read(gridFactory(), fileName, boundaryMarkers_, elementMarkers_, vertexMarkers_, verbose, boundarySegments);
            gridPtr() = std::shared_ptr<Grid>(gridFactory().createGrid());
        }
        else
        {
            Dune::GmshReader<Grid>::read(gridFactory(), fileName, verbose, boundarySegments);
            gridPtr() = std::shared_ptr<Grid>(gridFactory().createGrid());
        }
    }

    /*!
     * \brief Makes a grid from a DGF file. This is used by grid managers that only support DGF.
     */
    static void makeGridFromDgfFile(const std::string& fileName)
    {
        // We found a file in the input file...does it have a supported extension?
        const std::string extension = getFileExtension(fileName);
        if(extension != "dgf")
            DUNE_THROW(Dune::IOError, "Grid type " << Dune::className<Grid>() << " only supports DGF (*.dgf) but the specified filename has extension: *."<< extension);

        enableDgfGridPointer_ = true;
        dgfGridPtr() = Dune::GridPtr<Grid>(fileName.c_str(), Dune::MPIHelper::getCommunicator());
    }

    /*!
     * \brief The cell types for structured grids
     */
    enum CellType {Simplex, Cube};

    /*!
     * \brief Makes a structured cube grid using the structured grid factory
     */
    template <int dim, int dimworld>
    static void makeStructuredGrid(CellType cellType)
    {
        // The required parameters
        typedef Dune::FieldVector<typename Grid::ctype, dimworld> GlobalPosition;
        const GlobalPosition upperRight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, UpperRight);

        // The optional parameters (they have a default)
        GlobalPosition lowerLeft(0.0);
        try { lowerLeft = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, LowerLeft); }
        catch (Dumux::ParameterException &e) { }

        typedef std::array<unsigned int, dim> CellArray;
        CellArray cells;
        std::fill(cells.begin(), cells.end(), 1);
        try { cells = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, CellArray, Grid, Cells); }
        catch (Dumux::ParameterException &e) { }

        // make the grid
        if (cellType == CellType::Cube)
        {
            gridPtr() = Dune::StructuredGridFactory<Grid>::createCubeGrid(lowerLeft, upperRight, cells);
        }
        if (cellType == CellType::Simplex)
        {
            gridPtr() = Dune::StructuredGridFactory<Grid>::createSimplexGrid(lowerLeft, upperRight, cells);
        }
    }

    /*!
     * \brief Refines a grid after construction if GridParameterGroup.Refinement is set in the input file
     */
    static void maybeRefineGrid()
    {
        try {
            const int level = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, Refinement);
            grid().globalRefine(level);
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }
    }

    /*!
    * \brief A state variable if the DGF Dune::GridPtr has been enabled.
    *        It is always enabled if a DGF grid file was used to create the grid.
    */
    static bool enableDgfGridPointer_;

    /*!
    * \brief A state variable if domain markers have been read from a Gmsh file.
    */
    static bool enableGmshDomainMarkers_;

    /*!
    * \brief Element and boundary domain markers obtained from Gmsh physical entities
    *        They map from element indices / boundary ids to the physical entity number
    */
    static std::vector<int> elementMarkers_;
    static std::vector<int> boundaryMarkers_;
    static std::vector<int> vertexMarkers_;
};

template <class TypeTag, class Grid>
bool GridCreatorBase<TypeTag, Grid>::enableDgfGridPointer_ = false;

template <class TypeTag, class Grid>
bool GridCreatorBase<TypeTag, Grid>::enableGmshDomainMarkers_ = false;

template <class TypeTag, class Grid>
std::vector<int> GridCreatorBase<TypeTag, Grid>::elementMarkers_;

template <class TypeTag, class Grid>
std::vector<int> GridCreatorBase<TypeTag, Grid>::boundaryMarkers_;

template <class TypeTag, class Grid>
std::vector<int> GridCreatorBase<TypeTag, Grid>::vertexMarkers_;

/*!
 * \brief Provides the grid creator implementation for all supported grid managers that constructs a grid
 *        from information in the input file. This class is specialised below for all
 *        supported grid managers. It inherits the functionality of the base class.
 */
template <class TypeTag, class Grid>
class GridCreatorImpl : public GridCreatorBase<TypeTag, Grid>
{
public:
    /*!
     * \brief Make the grid. This is implemented by specializations of this class.
     */
    static void makeGrid()
    {
        DUNE_THROW(Dune::NotImplemented,
            "The GridCreator for grid type " << Dune::className<Grid>() << " is not implemented! Consider providing your own GridCreator.");
    }
};

/*!
 * \brief Provides the grid creator (this is the class called by the user) for all supported grid managers that constructs a grid
 *        from information in the input file. This class is specialised below for all
 *        supported grid managers. This is just an alias to avoid the unnecessary template argument.
 */
template <class TypeTag>
using GridCreator = GridCreatorImpl<TypeTag, typename GET_PROP_TYPE(TypeTag, Grid)>;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Specializations //////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
 * \brief Helper class for determining the default overlap in case of parallel yasp grids
 */
template <class TypeTag>
class YaspOverlapHelper
{
public:
    // trick to set overlap different for implicit models...
    template<class T = TypeTag>
    static typename std::enable_if<Properties::propertyDefined<T, T, PTAG_(ImplicitIsBox)>::value, int>::type
    getOverlap()
    {
        // the default is dependent on the discretization:
        // our box models only work with overlap 0
        // our cc models only work with overlap > 0
        static const int isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox);
        int overlap = isBox ? 0 : 1;
        try { overlap = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, Overlap);}
        catch (Dumux::ParameterException &e) { }

        if (isBox && overlap != 0)
            DUNE_THROW(Dune::NotImplemented, "Parallel overlapping grids for box models.");
        if (!isBox && overlap < 1)
            DUNE_THROW(Dune::NotImplemented, "Parallel non-overlapping grids for cc models.");

        return overlap;
    }

    //... then for other models (e.g. sequential)
    template<class T = TypeTag>
    static typename std::enable_if<!Properties::propertyDefined<T, T, PTAG_(ImplicitIsBox)>::value, int>::type
    getOverlap()
    {
        int overlap = 1;
        try { overlap = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, Overlap);}
        catch (Dumux::ParameterException &e) { }
        return overlap;
    }

};

/*!
 * \brief Provides a grid creator for OneDGrids
 *        from information in the input file
 *
 * All keys are expected to be in group GridParameterGroup.
 * The following keys are recognized:
 * - LeftBoundary : start coordinate
 * - RightBoundary : end coordinate
 * - Cells : the number of cell
 * - RefinementType : local or copy
 * - Refinement : the number of global refines to apply initially.
 *
 */
template<class TypeTag>
class GridCreatorImpl<TypeTag, Dune::OneDGrid>
          : public GridCreatorBase<TypeTag, Dune::OneDGrid>
{
public:
    typedef typename Dune::OneDGrid Grid;
    typedef GridCreatorBase<TypeTag, Grid> ParentType;

    /*!
     * \brief Make the grid. This is implemented by specializations of this method.
     */
    static void makeGrid()
    {
        // First try to create it from a DGF file in GridParameterGroup.File
        try {
            const std::string fileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, File);
            ParentType::makeGridFromDgfFile(fileName);
            postProcessing_();
            return;
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        // Look for the necessary keys to construct from the input file
        try {
            // The required parameters
            typedef typename Grid::ctype Coordinate;
            const Coordinate leftBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Coordinate, Grid, LeftBoundary);
            const Coordinate rightBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Coordinate, Grid, RightBoundary);

            // The optional parameters
            int cells = 1;
            try { cells = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, Cells);}
            catch (Dumux::ParameterException &e) { }

            ParentType::gridPtr() = std::make_shared<Grid>(cells, leftBoundary, rightBoundary);
            postProcessing_();
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        // Look for the necessary keys to construct from the input file with just a coordinates vector
        try {
            // The required parameters
            typedef std::vector<typename Grid::ctype> Coordinates;
            const Coordinates coordinates = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Coordinates, Grid, Coordinates);
            // make the grid
            ParentType::gridPtr() = std::make_shared<Grid>(coordinates);
            postProcessing_();
        }
        catch (Dumux::ParameterException &e) {
            DUNE_THROW(Dumux::ParameterException, "Please supply the mandatory parameters "
                                         << "Grid.LeftBoundary, "
                                         << "Grid.RightBoundary or "
                                         << "Grid.Coordinates or a grid file in "
                                         << "Grid.File.");
        }
        catch (...) { throw; }
    }

private:
    /*!
     * \brief Do some operatrion after making the grid, like global refinement
     */
    static void postProcessing_()
    {
        // Check for refinement type
        std::string refType = "Local";
        try { refType = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, RefinementType);
              if (refType != "Local" && refType != "Copy")
                   DUNE_THROW(Dune::IOError, "OneGrid only supports 'Local' or 'Copy' as refinment type. Not '"<< refType<<"'!");
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        if (refType == "Local")
            ParentType::grid().setRefinementType(Dune::OneDGrid::RefinementType::LOCAL);
        if (refType == "Copy")
            ParentType::grid().setRefinementType(Dune::OneDGrid::RefinementType::COPY);

        // Check if should refine the grid
        ParentType::maybeRefineGrid();
    }
};

#if HAVE_UG

/*!
 * \brief Provides a grid creator for UGGrids
 *        from information in the input file
 *
 * All keys are expected to be in group GridParameterGroup.

 * The following keys are recognized:
 * - File : A DGF or gmsh file to load from, type detection by file extension
 * - LowerLeft : lowerLeft corner of a structured grid
 * - UpperRight : upperright corner of a structured grid
 * - Cells : number of elements in a structured grid
 * - CellType : "Cube" or "Simplex" to be used for structured grids
 * - Refinement : the number of global refines to perform
 * - Verbosity : whether the grid construction should output to standard out
 * - HeapSize: The heapsize used to allocate memory
 * - BoundarySegments : whether to insert boundary segments into the grid
 *
 */
template<class TypeTag, int dim>
class GridCreatorImpl<TypeTag, Dune::UGGrid<dim> >
          : public GridCreatorBase<TypeTag, Dune::UGGrid<dim> >
{
public:
    typedef typename Dune::UGGrid<dim> Grid;
    typedef GridCreatorBase<TypeTag, Grid> ParentType;

    /*!
     * \brief Make the UGGrid.
     */
    static void makeGrid()
    {
        // First try to create it from a DGF or msh file in GridParameterGroup.File
        try {
            const std::string fileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, File);
            preProcessing_();
            ParentType::makeGridFromFile(fileName);
            postProcessing_();
            return;
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        // Then look for the necessary keys to construct from the input file
        try {
            preProcessing_();
            // Check for cell type
            std::string cellType = "Cube";
            try { cellType = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, CellType);
                if (cellType != "Cube" && cellType != "Simplex")
                    DUNE_THROW(Dune::IOError, "UGGrid only supports 'Cube' or 'Simplex' as cell type. Not '"<< cellType<<"'!");
            }
            catch (Dumux::ParameterException &e) {}
            catch (...) { throw; }

            // make the grid
            if (cellType == "Cube")
                ParentType::template makeStructuredGrid<dim, dim>(ParentType::CellType::Cube);
            if (cellType == "Simplex")
                ParentType::template makeStructuredGrid<dim, dim>(ParentType::CellType::Simplex);
            postProcessing_();
        }
        catch (Dumux::ParameterException &e) {
                DUNE_THROW(Dumux::ParameterException, "Please supply the mandatory parameters "
                                              << "Grid.UpperRight or a grid file in "
                                              << "Grid.File.");
        }
        catch (...) { throw; }
    }

private:
    /*!
     * \brief Do some operatrion before making the grid
     */
    static void preProcessing_()
    {
        bool setDefaultHeapSize = true;
        unsigned defaultHeapSize;
        try { defaultHeapSize = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, unsigned, Grid, HeapSize);}
        catch (Dumux::ParameterException &e) { setDefaultHeapSize = false; }

        if(setDefaultHeapSize)
            Grid::setDefaultHeapSize(defaultHeapSize);
    }

    /*!
     * \brief Do some operatrion after making the grid, like global refinement
     */
    static void postProcessing_()
    {
        // Check for refinement type
        std::string refType = "Local";
        try { refType = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, RefinementType);
              if (refType != "Local" && refType != "Copy")
                   DUNE_THROW(Dune::IOError, "UGGrid only supports 'Local' or 'Copy' as refinment type. Not '"<< refType<<"'!");
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        if (refType == "Local")
            ParentType::grid().setRefinementType(Dune::UGGrid<dim>::RefinementType::LOCAL);
        if (refType == "Copy")
            ParentType::grid().setRefinementType(Dune::UGGrid<dim>::RefinementType::COPY);

        // Check for closure type
        std::string closureType = "Green";
        try { closureType = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, ClosureType);
              if (closureType != "None" && closureType != "Green")
                   DUNE_THROW(Dune::IOError, "UGGrid only supports 'Green' or 'None' as closure type. Not '"<< closureType<<"'!");
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        if (closureType == "Green")
            ParentType::grid().setClosureType(Dune::UGGrid<dim>::ClosureType::GREEN);
        if (closureType == "None")
            ParentType::grid().setClosureType(Dune::UGGrid<dim>::ClosureType::NONE);

        // Check if should refine the grid
        ParentType::maybeRefineGrid();
    }
};

#endif // HAVE_UG

#if HAVE_DUNE_ALUGRID

/*!
 * \brief Provides a grid creator for Dune ALUGrids
 *        from information in the input file
 *
 * All keys are expected to be in group GridParameterGroup.

 * The following keys are recognized:
 * - File : A DGF or gmsh file to load from, type detection by file extension
 * - LowerLeft : lowerLeft corner of a structured grid
 * - UpperRight : upperright corner of a structured grid
 * - Cells : number of elements in a structured grid
 * - Refinement : the number of global refines to perform
 * - Verbosity : whether the grid construction should output to standard out
 * - BoundarySegments : whether to insert boundary segments into the grid
 *
 */
template<class TypeTag, int dim, int dimworld, Dune::ALUGridElementType elType, Dune::ALUGridRefinementType refinementType>
class GridCreatorImpl<TypeTag, Dune::ALUGrid<dim, dimworld, elType, refinementType> >
          : public GridCreatorBase<TypeTag, Dune::ALUGrid<dim, dimworld, elType, refinementType> >
{
public:
    typedef typename Dune::ALUGrid<dim, dimworld, elType, refinementType> Grid;
    typedef GridCreatorBase<TypeTag, Grid> ParentType;

    /*!
     * \brief Make the grid. This is implemented by specializations of this method.
     */
    static void makeGrid()
    {
#if HAVE_DUNE_ALUGRID
        // First check if a restart for an adaptive grid is required
        try {
            if (GET_PROP_VALUE(TypeTag, AdaptiveGrid))
            {
                typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
                Scalar restartTime = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, Restart);
                // we came until here so the restart key was found. Restore the grid.
                int rank = 0;
#if HAVE_MPI
                MPI_Comm_rank(Dune::MPIHelper::getCommunicator(), &rank);
#endif
                try {
                    std::string name = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
                    std::ostringstream oss;
                    oss << name << "_time=" << restartTime << "_rank=" << rank << ".grs";
                    std::cout << "Restoring an ALUGrid from " << oss.str() << std::endl;
                    ParentType::gridPtr() = std::shared_ptr<Grid>(Dune::BackupRestoreFacility<Grid>::restore(oss.str()));
                    return;
                }
                catch (Dumux::ParameterException &e)
                {
                    std::cerr << e.what() << std::endl;
                    std::cerr << "Restart functionality for an adaptive grid requested, but failed." << std::endl;
                    std::cerr << "Did you forget to provide Problem.Name in your .input file?" << std::endl;
                    throw;
                }
            }
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }
#endif

        // Then try to create it from a DGF or msh file in GridParameterGroup.File
        try {
            const std::string fileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, File);
            ParentType::makeGridFromFile(fileName);
            ParentType::maybeRefineGrid();
            return;
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        // Then look for the necessary keys to construct from the input file
        try {
            // make a structured grid
            if (elType == Dune::cube)
                ParentType::template makeStructuredGrid<dim, dimworld>(ParentType::CellType::Cube);
            if (elType == Dune::simplex)
                ParentType::template makeStructuredGrid<dim, dimworld>(ParentType::CellType::Simplex);
            ParentType::maybeRefineGrid();
        }
        catch (Dumux::ParameterException &e) {
                DUNE_THROW(Dumux::ParameterException, "Please supply the mandatory parameters "
                                              << "Grid.UpperRight or a grid file in "
                                              << "Grid.File.");
        }
        catch (...) { throw; }
    }

    static void makeGridFromGmshFile(const std::string& fileName, bool verbose, bool boundarySegments, bool domainMarkers)
    {
        if(domainMarkers)
        {
            // only filll the factory for rank 0
            if (Dune::MPIHelper::getCollectiveCommunication().rank() == 0)
                Dune::GmshReader<Grid>::read(ParentType::gridFactory(), fileName, ParentType::boundaryMarkers_, ParentType::elementMarkers_, ParentType::vertexMarkers_, verbose, boundarySegments));

            ParentType::gridPtr() = std::shared_ptr<Grid>(ParentType::gridFactory().createGrid());
        }
        else
        {
            // only filll the factory for rank 0
            if (Dune::MPIHelper::getCollectiveCommunication().rank() == 0)
                Dune::GmshReader<Grid>::read(ParentType::gridFactory(), fileName, verbose, boundarySegments);

            ParentType::gridPtr() = std::shared_ptr<Grid>(ParentType::gridFactory().createGrid());
        }
    }
};

#endif // HAVE_DUNE_ALUGRID

#if HAVE_DUNE_FOAMGRID

/*!
 * \brief Provides a grid creator for FoamGrids
 *        from information in the input file
 *
 * All keys are expected to be in group GridParameterGroup.

 * The following keys are recognized:
 * - File : A DGF or gmsh file to load from, type detection by file extension
 * - Verbosity : whether the grid construction should output to standard out
 * - LowerLeft : lowerLeft corner of a structured grid
 * - UpperRight : upperright corner of a structured grid
 * - Cells : number of elements in a structured grid
 *
 */
template<class TypeTag, int dim, int dimworld>
class GridCreatorImpl<TypeTag, Dune::FoamGrid<dim, dimworld> >
          : public GridCreatorBase<TypeTag, Dune::FoamGrid<dim, dimworld> >
{
public:
    typedef typename Dune::FoamGrid<dim, dimworld> Grid;
    typedef GridCreatorBase<TypeTag, Grid> ParentType;

    /*!
     * \brief Make the grid. This is implemented by specializations of this method.
     */
    static void makeGrid()
    {
        // First try to create it from a DGF or msh file in GridParameterGroup.File
        try {
            const std::string fileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, File);
            ParentType::makeGridFromFile(fileName);
            ParentType::maybeRefineGrid();
            return;
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        // Then look for the necessary keys to construct a structured grid from the input file
        try {
            ParentType::template makeStructuredGrid<dim, dimworld>(ParentType::CellType::Simplex);
            ParentType::maybeRefineGrid();
        }
        catch (Dumux::ParameterException &e) {
                DUNE_THROW(Dumux::ParameterException, "Please supply the mandatory parameters "
                                              << "Grid.UpperRight or a grid file in "
                                              << "Grid.File.");
        }
        catch (...) { throw; }
    }
};

/*!
 * \brief Provides a grid creator for FoamGrids of dim 1
 *        from information in the input file
 *
 * All keys are expected to be in group GridParameterGroup.

 * The following keys are recognized:
 * - File : A DGF or gmsh file to load from, type detection by file extension
 * - Verbosity : whether the grid construction should output to standard out
 * - LowerLeft : lowerLeft corner of a structured grid
 * - UpperRight : upperright corner of a structured grid
 * - Cells : number of elements in a structured grid
 *
 */
template<class TypeTag, int dimworld>
class GridCreatorImpl<TypeTag, Dune::FoamGrid<1, dimworld> >
          : public GridCreatorBase<TypeTag, Dune::FoamGrid<1, dimworld> >
{
public:
    typedef typename Dune::FoamGrid<1, dimworld> Grid;
    typedef GridCreatorBase<TypeTag, Grid> ParentType;

    /*!
     * \brief Make the grid. This is implemented by specializations of this method.
     */
    static void makeGrid()
    {
        // First try to create it from a DGF or msh file in GridParameterGroup.File
        try {
            const std::string fileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Grid, File);
            ParentType::makeGridFromFile(fileName);
            ParentType::maybeRefineGrid();
            return;
        }
        catch (Dumux::ParameterException &e) {}
        catch (...) { throw; }

        // Then look for the necessary keys to construct a structured grid from the input file
        try {
            // The required parameters
            typedef Dune::FieldVector<typename Grid::ctype, dimworld> GlobalPosition;
            const GlobalPosition upperRight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, UpperRight);

            // The optional parameters (they have a default)
            GlobalPosition lowerLeft(0.0);
            try { lowerLeft = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, LowerLeft); }
            catch (Dumux::ParameterException &e) { }

            typedef std::array<unsigned int, 1> CellArray;
            CellArray cells;
            std::fill(cells.begin(), cells.end(), 1);
            try { cells = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, CellArray, Grid, Cells); }
            catch (Dumux::ParameterException &e) { }

            // make the grid (structured interval grid in dimworld space)
            Dune::GeometryType geomType(1);

            // create a step vector
            GlobalPosition step = upperRight;
            step -= lowerLeft, step /= cells[0];

            // create the vertices
            GlobalPosition globalPos = lowerLeft;
            for (unsigned int vIdx = 0; vIdx <= cells[0]; vIdx++, globalPos += step)
                ParentType::gridFactory().insertVertex(globalPos);

            // create the cells
            for(unsigned int eIdx = 0; eIdx < cells[0]; eIdx++)
                ParentType::gridFactory().insertElement(geomType, {eIdx, eIdx+1});

            ParentType::gridPtr() = std::shared_ptr<Grid>(ParentType::gridFactory().createGrid());
            ParentType::maybeRefineGrid();
        }
        catch (Dumux::ParameterException &e) {
                DUNE_THROW(Dumux::ParameterException, "Please supply the mandatory parameters "
                                              << "Grid.UpperRight or a grid file in "
                                              << "Grid.File.");
        }
        catch (...) { throw; }
    }
};

#endif // HAVE_DUNE_FOAMGRID

} // namespace Dumux

#endif
