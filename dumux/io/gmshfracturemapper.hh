/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_GMSH_FRACTURE_MAPPER_HH
#define DUMUX_GMSH_FRACTURE_MAPPER_HH

#include <dune/common/version.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dumux/common/propertysystem.hh>

namespace Dumux
{

template<class TypeTag>
class GmshFractureMapper
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView)  GridView;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator)  GridCreator;
public:
    // mapper: one data element in every entity
    template<int dim>
    struct FaceLayout
    {
        bool contains (Dune::GeometryType geomType)
        {
            return geomType.dim() == dim-1;
        }
    };
    typedef typename GridView::ctype DT;
    enum {dim = GridView::dimension};
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, FaceLayout> FaceMapper;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGVertexLayout> VertexMapper;

public:
    GmshFractureMapper (const GridView& gridView)
    : gridView_(gridView),
      faceMapper_(gridView),
      vertexMapper_(gridView)
    {}

    void map()
    {
        //call the new_read art
        int numVertices = gridView_.size(dim);
        int nEdges = gridView_.size(1);
        //The vertexes which are located on fractures
        isFractureVertex_.resize(numVertices);
        std::fill(isFractureVertex_.begin(), isFractureVertex_.end(), false);

        //The edge which are fractures
        isFractureEdge_.resize(nEdges);
        std::fill(isFractureEdge_.begin(), isFractureEdge_.end(), false);

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(; eIt != eEndIt; ++eIt)
        {
             const auto& element = *eIt;

             Dune::GeometryType geomType = element.geometry().type();

             const typename Dune::ReferenceElementContainer<DT,dim>::value_type&
                 refElement = Dune::ReferenceElements<DT,dim>::general(geomType);

              // Loop over element faces
              for (int i = 0; i < refElement.size(1); i++)
              {
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                  int indexFace = faceMapper_.subIndex(element, i, 1);
#else 
                  int indexFace = faceMapper_.map(element, i, 1);
#endif
                  /*
                  * it maps the local element vertices "localV1Idx" -> indexVertex1
                  * then it gets the coordinates of the nodes in the ART file and
                  * by comparing them with the ones in the DUNE grid maps them too.
                  */
                  int localV1Idx = refElement.subEntity(i, 1, 0, dim);
                  int localV2Idx = refElement.subEntity(i, 1, 1, dim);
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                  int indexVertex1 = vertexMapper_.subIndex(element, localV1Idx, dim);
#else 
                  int indexVertex1 = vertexMapper_.map(element, localV1Idx, dim);
#endif

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                  int indexVertex2 = vertexMapper_.subIndex(element, localV2Idx, dim);
#else 
                  int indexVertex2 = vertexMapper_.map(element, localV2Idx, dim);
#endif

                  if (GridCreator::getVertexMarker(indexVertex1) > 0)
                      isFractureVertex_[indexVertex1] = true;

                  if (GridCreator::getVertexMarker(indexVertex2) > 0)
                      isFractureVertex_[indexVertex2] = true;

                  if (GridCreator::getVertexMarker(indexVertex1) > 0
                      && GridCreator::getVertexMarker(indexVertex2) > 0)
                      isFractureEdge_[indexFace] = true;
            }
         }
    }

    bool isFractureVertex(unsigned int i) const
    {
        return isFractureVertex_[i];
    }

    bool isFractureEdge(unsigned int i) const
    {
        return isFractureEdge_[i];
    }

    const FaceMapper& faceMapper() const
    {
        return faceMapper_;
    }

    const VertexMapper& vertexMapper() const
    {
        return vertexMapper_;
    }

private:
    const GridView gridView_;
    FaceMapper faceMapper_;
    VertexMapper vertexMapper_;
    std::vector<bool> isFractureVertex_;
    std::vector<bool> isFractureEdge_;
};

} // end namespace Dumux
#endif // DUMUX_GMSH_FRACTURE_MAPPER_HH
