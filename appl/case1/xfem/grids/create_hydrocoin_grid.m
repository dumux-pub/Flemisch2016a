nx = 44;
ny = 30;

if rem(nx, 4) > 0
  disp('nx has to be divisible by 4.')
  return
end

filename = sprintf('hydrocoin_%d_%d.dgf', nx, ny)
dgffile = fopen(filename, 'w');

x1 = 400; y1 = 100;
x2 = 1500; y2 = -1000;
x3 = 1200; y3 = 100;
x4 = 1000; y4 = -1000;
ix = ((x1*y2 - y1*x2)*(x3 - x4) - (x1 - x2)*(x3*y4 - y3*x4)) ...
     / ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4));
iy = ((x1*y2 - y1*x2)*(y3 - y4) - (y1 - y2)*(x3*y4 - y3*x4)) ...
     / ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4));


fprintf(dgffile, 'DGF\nVertex\n');
for j = 0:ny
  s = j/ny;
  for i = 0:nx
    r = i/nx;
    x = 1600*r;

    if r < 0.25
      y1 = 50*(3 - 4*r);
    elseif r < 0.5
      y1 = 50*(1 + 4*r);
    elseif r < 0.75
      y1 = 50*(5 - 4*r);
    else
      y1 = 50*(-1 + 4*r);
    end
    y = -1000 + (y1 + 1000)*s;

    fprintf(dgffile, '%f\t%f\n', x, y);
  end
end

fprintf(dgffile, '#\nCube\n');
for j = 1:ny
  for i = 1:nx
    ll = (j - 1)*(nx + 1) + i - 1;
    fprintf(dgffile, '%d\t%d\t%d\t%d\n', ll, ll+1, ll+nx+1, ll+nx+2);
  end
end

fprintf(dgffile, '#\nBoundaryDomain\ndefault 1\n#\n#\n');

fclose(dgffile);
