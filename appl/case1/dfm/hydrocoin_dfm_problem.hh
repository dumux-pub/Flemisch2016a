/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_HYDROCOIN_DFM_PROBLEM_HH
#define DUMUX_HYDROCOIN_DFM_PROBLEM_HH

#include <dune/grid/uggrid.hh>

#include <dumux/implicit/2pdfm/2pdfmmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/material/components/unit.hh>
#include <dumux/io/gridcreator.hh>

#include "hydrocoin_dfm_spatialparams.hh"

namespace Dumux
{

template <class TypeTag>
class HydrocoinDfmProblem;

namespace Properties
{
NEW_TYPE_TAG(HydrocoinDfmProblem, INHERITS_FROM(BoxTwoPDFM, HydrocoinDfmSpatialParams));

// Set the grid type
SET_TYPE_PROP(HydrocoinDfmProblem, Grid, Dune::UGGrid<2>);

// set the GridCreator property
SET_TYPE_PROP(HydrocoinDfmProblem, GridCreator, Dumux::GridCreator<TypeTag>);

// Set the problem property
SET_TYPE_PROP(HydrocoinDfmProblem, Problem, Dumux::HydrocoinDfmProblem<TypeTag>);

// Set the wetting phase
SET_PROP(HydrocoinDfmProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(HydrocoinDfmProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(HydrocoinDfmProblem, ProblemEnableGravity, false);
}

template <class TypeTag >
class HydrocoinDfmProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, NonwettingPhase) NonwettingPhase;

    enum {
        // primary variable indices
        pwIdx = Indices::pwIdx,
        snIdx = Indices::snIdx,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    HydrocoinDfmProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), useInterfaceCondition_(true)
    {
        eps_ = 1e-6;
        temperature_ = 273.15 + 20.0; // -> 20°C
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
    }

    const char *name() const
    {
        return name_.c_str();
    }

    Scalar temperature() const
    { return temperature_; };

    void sourceAtPos(PrimaryVariables &source,
                const GlobalPosition &globalPos) const
    {
        source    = 0;
    }

    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        if (notOnOtherBoundary_(globalPos) ){
            values.setAllDirichlet();
        }
        else {
            values.setAllNeumann();
        }
    }

    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        initialAtPos(values, globalPos);
    }

    void neumannAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }

    void initialAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        values[pwIdx] = globalPos[1];
        values[snIdx] = 0.0;
    }

    void addOutputVtkFields()
    {
        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

        unsigned numDofs = this->model().numDofs();

        ScalarField *isFracture = this->resultWriter().allocateManagedBuffer(numDofs);

        VertexIterator vIt = this->gridView().template begin<dim>();
        VertexIterator vEndIt = this->gridView().template end<dim>();
        for (; vIt != vEndIt; ++vIt)
        {
            int vIdx = this->vertexMapper().map(*vIt);
            (*isFracture)[vIdx] = GridCreator::getVertexMarker(vIdx);
        }

        this->resultWriter().attachDofData(*isFracture, "isFracture", true);
    }

    bool useInterfaceCondition() const
    {
        return useInterfaceCondition_;
    }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return ( (globalPos[0] < eps_) && (globalPos[1] < 150 - eps_) );
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return ( (globalPos[0] > 1600 - eps_) && (globalPos[1] < 150 - eps_) );
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return (globalPos[1] < -1000 + eps_);
    }

    bool notOnOtherBoundary_(const GlobalPosition &globalPos) const
    {
        return (!onLeftBoundary_(globalPos) && !onRightBoundary_(globalPos) && !onLowerBoundary_(globalPos) );
    }

    Scalar temperature_;
    Scalar eps_;
    bool useInterfaceCondition_;
    std::string name_;
};
} //end namespace

#endif // DUMUX_HYDROCOIN_DFM_PROBLEM_HH
