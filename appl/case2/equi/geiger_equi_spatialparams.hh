// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GEIGER_EQUI_SPATIALPARAMS_HH
#define DUMUX_GEIGER_EQUI_SPATIALPARAMS_HH

#include <dumux/decoupled/common/decoupledproperties.hh>
#include <dumux/material/spatialparams/fvspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class GeigerEquiSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(GeigerEquiSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(GeigerEquiSpatialParams, SpatialParams, Dumux::GeigerEquiSpatialParams<TypeTag>);

// Set the material law
SET_PROP(GeigerEquiSpatialParams, MaterialLaw)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef LinearMaterial<Scalar> RawMaterialLaw;
public:
    typedef EffToAbsLaw<RawMaterialLaw> type;
};
}

template<class TypeTag>
class GeigerEquiSpatialParams: public FVSpatialParams<TypeTag>
{
    typedef FVSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> ElementMapper;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP(TypeTag, SolutionTypes)::ScalarSolution ScalarSolution;
    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    const FieldMatrix& intrinsicPermeabilityAtPos (const GlobalPosition& globalPos) const
    {
        if ( (globalPos[0]>=0.5-0.5e-4 && globalPos[0]<=0.5+0.5e-4)
            || ( globalPos[1] >= 0.5-0.5e-4 && globalPos[1] <= 0.5+0.5e-4) )
            return kFracture_;//largest cross
        else if ( globalPos[0] >= 0.5 && globalPos[1] >= 0.5)
        {
            //second largest cross
            if ( (globalPos[0]>=0.75-0.5e-4 && globalPos[0]<=0.75+0.5e-4)
                || ( globalPos[1] >= 0.75-0.5e-4 && globalPos[1] <= 0.75+0.5e-4) )
            {
                return kFracture_;
            }
            //smallest cross
            else if ( globalPos[0] <= 0.75 && globalPos[1] <= 0.75)
            {
                if ( (globalPos[0]>=0.625-0.5e-4 && globalPos[0]<=0.625+0.5e-4)
                    || ( globalPos[1] >= 0.625-0.5e-4 && globalPos[1] <= 0.625+0.5e-4) )
                {
                    return kFracture_;
                }
            }
        }
        return kMatrix_;
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }

    const MaterialLawParams& materialLawParams(const Element &element) const
    {
            return materialLawParams_;
    }

    template<class Writer>
    void addOutputVtkFields(Writer& writer)
    {
        ScalarSolution *perm = writer.allocateManagedBuffer(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            int globalIdx = mapper_.map(*eIt);
            const auto& k = intrinsicPermeabilityAtPos(eIt->geometry().center());
            (*perm)[globalIdx][0] = k[0][0];
        }

        writer.attachCellData(*perm, "permeability");
    }

    GeigerEquiSpatialParams(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), mapper_(gridView)
    {
        kMatrix_[0][0] = 1.0;
        kMatrix_[0][1] = 0.0;
        kMatrix_[1][0] = 0.0;
        kMatrix_[1][1] = 1.0;

        auto kf = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, KFracture);
        kFracture_[0][0] = kf;
        kFracture_[0][1] = 0.0;
        kFracture_[1][0] = 0.0;
        kFracture_[1][1] = kf;
    }

private:
    const GridView gridView_;
    MaterialLawParams materialLawParams_;
    FieldMatrix kFracture_;
    FieldMatrix kMatrix_;
    ElementMapper mapper_;
};

} // end namespace
#endif
