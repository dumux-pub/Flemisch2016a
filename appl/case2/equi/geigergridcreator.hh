// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GEIGER_GRID_CREATOR_HH
#define DUMUX_GEIGER_GRID_CREATOR_HH

#include <dune/grid/utility/structuredgridfactory.hh>

#include <dumux/common/basicproperties.hh>

namespace Dumux
{

namespace Properties
{
NEW_PROP_TAG(Scalar);
NEW_PROP_TAG(Grid);
}

template <class TypeTag>
class GeigerGridCreator
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid)  Grid;
    typedef Grid* GridPointer;

    enum { dim = Grid::dimension };

public:
    /*!
     * \brief Create the Grid
     */
    static void makeGrid()
    {
        int numVerticesOneD = 1085;
        std::vector<Scalar> oneDPositions(numVerticesOneD);

        oneDPositions[0] = 0;
        for (int i = 1; i < oneDPositions.size(); ++i)
        {
            Scalar h = 1e-3;
            if ((i > 499 && i < 509) || (i > 520 && i < 530)
                || (i > 652 && i < 662) || (i > 673 && i < 683)
                || (i > 805 && i < 815) || (i > 826 && i < 836))
                h = 1e-4;
            else if ((i > 509 && i < 520) || (i > 662 && i < 673) || (i > 815 && i < 826))
                h = 1e-5;
            else if (i == 509 || i == 520 || i == 662 || i == 673 || i == 815 || i == 826)
                h = 5e-5;

            oneDPositions[i] = oneDPositions[i-1] + h;
        }

        Dune::GridFactory<Grid> factory;

        for (int j = 0; j < oneDPositions.size(); ++j)
        {
            Dune::FieldVector<Scalar, dim> vertex(oneDPositions[j]);
            for (int i = 0; i < oneDPositions.size(); ++i)
            {
                vertex[0] = oneDPositions[i];
                factory.insertVertex(vertex);
            }
        }

        for (int j = 0; j < numVerticesOneD-1; ++j)
        {
            for (int i = 0; i < numVerticesOneD-1; ++i)
            {
                std::vector<unsigned int> vertices(4);

                vertices[0] = j*numVerticesOneD + i;
                vertices[1] = j*numVerticesOneD + i+1;
                vertices[2] = (j+1)*numVerticesOneD + i;
                vertices[3] = (j+1)*numVerticesOneD + i+1;

                factory.insertElement(Dune::GeometryType(Dune::GeometryType::cube,dim), vertices);
            }
        }

        grid_ = factory.createGrid();
    }

    /*!
     * \brief Returns a reference to the grid.
     */
    static Grid &grid()
    {
        return *grid_;
    }

    /*!
     * \brief Distributes the grid on all processes of a parallel
     *        computation.
     */
    static void loadBalance()
    {
        grid_->loadBalance();
    }

protected:
    static GridPointer grid_;
};

template <class TypeTag>
typename Dumux::GeigerGridCreator<TypeTag>::GridPointer GeigerGridCreator<TypeTag>::grid_;

}

#endif
