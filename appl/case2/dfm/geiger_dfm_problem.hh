/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_GEIGER_DFM_PROBLEM_HH
#define DUMUX_GEIGER_DFM_PROBLEM_HH

#include <dumux/implicit/2pdfm/2pdfmmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/material/components/unit.hh>
#include <dumux/io/artgridcreator.hh>

#include "geiger_dfm_spatialparams.hh"

namespace Dumux
{

template <class TypeTag>
class GeigerDfmProblem;

namespace Properties
{
NEW_TYPE_TAG(GeigerDfmProblem, INHERITS_FROM(BoxTwoPDFM, GeigerDfmSpatialParams));

SET_TYPE_PROP(GeigerDfmProblem, Grid, Dune::UGGrid<2>);

// set the GridCreator property
SET_TYPE_PROP(GeigerDfmProblem, GridCreator, Dumux::ArtGridCreator<TypeTag>);

// Set the problem property
SET_TYPE_PROP(GeigerDfmProblem, Problem, Dumux::GeigerDfmProblem<TypeTag>);

// Set the wetting phase
SET_PROP(GeigerDfmProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(GeigerDfmProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(GeigerDfmProblem, ProblemEnableGravity, false);
}

/*!
 * \ingroup TwoPDFMModel
 * \ingroup ImplicitTestProblems
 * \brief Soil contamination problem involving a DNAPL migration into a
 *        fully water saturated media
 *
 * This problem uses the \ref TwoPDFMModel.
 *
 * This problem should typically be simulated until \f$t_{\text{end}}
 * \approx 100\,000\;s\f$ is reached. A good choice for the initial time step
 * size is \f$t_{\text{inital}} = 1\;s\f$.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_2pDFM grids/defaultgrid.net 1e6 1</tt>
 */
template <class TypeTag >
class GeigerDfmProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, NonwettingPhase) NonwettingPhase;

    enum {
        // primary variable indices
        pwIdx = Indices::pwIdx,
        snIdx = Indices::snIdx,

        // equation indices
        contiNEqIdx = Indices::contiNEqIdx,

        // phase indices
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    GeigerDfmProblem(TimeManager &timeManager,
                       const GridView &gridView)
        : ParentType(timeManager, gridView),
          useInterfaceCondition_(true)
    {
        eps_ = 3e-6;
        temperature_ = 273.15 + 20; // -> 20°C
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
    }

    const char *name() const
    {
        return name_.c_str();
    }

    Scalar temperature() const
    { return temperature_; }

    void sourceAtPos(PrimaryVariables &source,
                const GlobalPosition &globalPos) const
    {
        source    = 0;
    }

    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        if (onRightBoundary_(globalPos)){
            values.setAllDirichlet();
        }
        else
        {
            values.setAllNeumann();
        }
    }

    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        values[pwIdx] = 1.0;
        values[snIdx] = 0.0;
    }

    void neumannAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        if(onLeftBoundary_(globalPos)){
            values[pwIdx] = -1.0;
            values[snIdx] =  0.0;
        }
        else{
            values = 0.0;
        }
    }

    void initialAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }

    bool useInterfaceCondition() const
    {
        return useInterfaceCondition_;
    }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->bBoxMax()[1] - eps_;
    }

    Scalar temperature_;
    Scalar eps_;

    bool useInterfaceCondition_;
    std::string name_;
};
} //end namespace

#endif // DUMUX_GEIGER_DFM_PROBLEM_HH
