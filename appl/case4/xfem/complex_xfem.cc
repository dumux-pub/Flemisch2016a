#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
//#include <dumux/common/quad.hh>
#include <iostream>
#include <boost/format.hpp>
#include <vector>
#include <map>
#include <string>
#include <typeinfo>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/static_assert.hh>
#include <dune/common/timer.hh>
#include <dune/common/parametertreeparser.hh>
//===============================================================
// dune-grid
//===============================================================
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/io/file/dgfparser.hh>
#if HAVE_ALBERTA
#include <dune/grid/albertagrid.hh>
#endif
#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif
#if HAVE_ALUGRID
#include <dune/grid/alugrid.hh>
#endif

#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/geometrygrid.hh>
#include <dune/grid/common/mcmgmapper.hh>
//===============================================================
// dune-istl
//===============================================================
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/superlu.hh>
//===============================================================
// dune-pdelab
//===============================================================
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/finiteelementmap/q1fem.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
//===============================================================
// dune-multidomain
//===============================================================
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/vtk.hh>
//===============================================================
// helper functions
//===============================================================
#include "dumux/xfem/common/helperfunctions.hh"
#include "dumux/xfem/common/fracturenetworkmanipulation.hh"
//===============================================================
// boundary conditions, basis functions, soil structure
//===============================================================
#include "dumux/xfem/common/globalboundary.hh"
#include "dumux/xfem/common/referenceproperties.hh"
#include "dumux/xfem/common/soil.hh"
#include "dumux/xfem/common/modifiedbasisfunction.hh"
#include "boundaryconditions.hh"
//===============================================================
// local operator
//===============================================================
//#include "dumux/xfem/common/matrixsolution.hh"
#include "dumux/xfem/common/solution.hh"
#include "dumux/xfem/common/fracturesolution.hh"
#include "dumux/xfem/common/matrixlocaloperatordarcy.hh"
#include "dumux/xfem/mono/matrixlocaloperatorenricheddarcy.hh"
#include "dumux/xfem/mono/fracturelocaloperatordarcy.hh"
#include "dumux/xfem/mono/couplinglocaloperatordarcy.hh"
#include "dumux/xfem/mono/sizeofcouplinglop.hh"
//===============================================================
// Main program with grid setup
//===============================================================
#include "dumux/xfem/common/artreader2d.hh"
#include "dumux/xfem/common/gridcreator.hh"
#include "dumux/xfem/common/gridcoupling.hh"
#include "dumux/xfem/mono/main.hh"
