/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_COMPLEX_DFM_SPATIALPARAMS_HH
#define DUMUX_COMPLEX_DFM_SPATIALPARAMS_HH

#include <dumux/implicit/2pdfm/2pdfmmodel.hh>
#include <dumux/io/gmshfracturemapper.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/spatialparams/implicitspatialparams.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class ComplexDfmSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(ComplexDfmSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(ComplexDfmSpatialParams, SpatialParams, Dumux::ComplexDfmSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(ComplexDfmSpatialParams, MaterialLaw)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef LinearMaterial<Scalar> type;
};
}

template<class TypeTag>
class ComplexDfmSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    ComplexDfmSpatialParams(const GridView& gridView)
    : ParentType(gridView),
      swrf_(0), swrm_(0), SnrF_(0), SnrM_(0), pdf_(1), pdm_(1), lambdaF_(1), lambdaM_(1),
      materialParams_(0, 0), fractureMapper_(gridView)
    {
        fractureMapper_.map();
    }

    /*!
     * \brief Intrinsic permability
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return Intrinsic permeability
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 int scvIdx) const
    { return 1.0; }

    /*!
     * \brief Intrinsic permeability of fractures.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     */
    Scalar intrinsicPermeabilityFracture(const Element &element,
                                         const FVElementGeometry &fvGeometry,
                                         int scvIdx) const
    {
        auto vIdx = fractureMapper_.vertexMapper().map(element, scvIdx, dim);

        if (GridCreator::getVertexMarker(vIdx) == 1)
            return 1e4;
        else if (GridCreator::getVertexMarker(vIdx) == 2)
            return 1e-4;
        else if (GridCreator::getVertexMarker(vIdx) == 3)
            return 1.9999999e-4; // harmonic mean of 1e4 and 1e-4
        else
            DUNE_THROW(Dune::NotImplemented, "permeability for vertex marker outside [1, 3]");
    }

    /*!
     * \brief Porosity
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return Porosity
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    { return 1.0; }

    /*!
     * \brief Porosity Fracture
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return Porosity Fracture
     */
    Scalar porosityFracture(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    { return 1.0; }

    /*!
     * \brief Function for defining the parameters needed by constitutive relationships (kr-sw, pc-sw, etc.).
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return the material parameters object
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                int scvIdx) const
    { return materialParams_; }

    /*!
     * \brief Function for defining the parameters needed by constitutive relationships (kr-sw, pc-sw, etc.).
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return the material parameters object of the Fracture
     */
    const MaterialLawParams& materialLawParamsFracture(const Element &element,
                                                    const FVElementGeometry &fvGeometry,
                                                    int scvIdx) const
    { return materialParams_; }

    /*!
     * \brief Checks whether vertex is a fracture.
     *
     * \param element The current element
     * \param localVertexIdx Vertex index to be checked
     */
    bool isVertexFracture(const Element &element, int localVertexIdx) const
    {
        int globalIdx = fractureMapper_.vertexMapper().map(element, localVertexIdx, dim);
        return fractureMapper_.isFractureVertex(globalIdx);
    }

    /*!
     * \brief Checks whether element edge is a fracture.
     *
     * \param element The current element
     * \param localFaceIdx Face index to be checked
     */
    bool isEdgeFracture(const Element &element, int localFaceIdx) const
    {
        int globalIdx = fractureMapper_.faceMapper().map(element, localFaceIdx, 1);
        return fractureMapper_.isFractureEdge(globalIdx);
    }

    /*!
     * \brief Returns the width of the fracture.
     *
     * \param element The current element
     * \param localFaceIdx Local face index of which the width is returned
     */
    Scalar fractureWidth(const Element &element, int localFaceIdx) const
    { return 1e-4; }

    // variables required in 2pdfmvolumevariables
    // no influence for the fully saturated scenario
    Scalar swrf_;
    Scalar swrm_;
    Scalar SnrF_;
    Scalar SnrM_;
    Scalar pdf_;
    Scalar pdm_;
    Scalar lambdaF_;
    Scalar lambdaM_;

private:
    MaterialLawParams materialParams_;
    Dumux::GmshFractureMapper<TypeTag> fractureMapper_;
};

} // end namespace
#endif // DUMUX_COMPLEX_DFM_SPATIALPARAMS_HH
