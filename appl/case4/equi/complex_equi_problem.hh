// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_COMPLEX_EQUI_PROBLEM_HH
#define DUMUX_COMPLEX_EQUI_PROBLEM_HH

#include <dune/grid/io/file/dgfparser/dgfug.hh>

#include <dumux/material/components/unit.hh>
#include <dumux/io/gridcreator.hh>

#include <dumux/decoupled/2p/diffusion/mimetic/mimeticpressureproperties2p.hh>
#include <dumux/decoupled/2p/diffusion/fvmpfa/lmethod/fvmpfal2dpressureproperties2p.hh>
#include <dumux/decoupled/2p/diffusion/diffusionproblem2p.hh>

#include "complex_equi_spatialparams.hh"

namespace Dumux
{
template<class TypeTag>
class ComplexEquiProblem;

namespace Properties
{
NEW_TYPE_TAG(ComplexEquiProblem, INHERITS_FROM(MimeticPressureTwoP, ComplexEquiSpatialParams));
//NEW_TYPE_TAG(ComplexEquiProblem, INHERITS_FROM(FvMpfaL2dPressureTwoP, ComplexEquiSpatialParams));

SET_TYPE_PROP(ComplexEquiProblem, Problem, Dumux::ComplexEquiProblem<TypeTag>);

SET_TYPE_PROP(ComplexEquiProblem, Grid, Dune::UGGrid<2>);

SET_TYPE_PROP(ComplexEquiProblem, GridCreator, GridCreator<TypeTag>);

SET_PROP(ComplexEquiProblem, WettingPhase){
private:
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
  typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

SET_PROP(ComplexEquiProblem, NonwettingPhase){
private:
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
  typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

SET_BOOL_PROP(ComplexEquiProblem, ProblemEnableGravity, false);

SET_TYPE_PROP(ComplexEquiProblem, LinearSolver, SuperLUBackend<TypeTag>);

} // end namespace Properties

template<class TypeTag>
class ComplexEquiProblem: public DiffusionProblem2P<TypeTag>
{
    typedef DiffusionProblem2P<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView)GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) NonwettingPhase;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        pwIdx = Indices::pwIdx,
        swIdx = Indices::swIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

public:
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::PrimaryVariables PrimaryVariables;
    typedef typename SolutionTypes::ScalarSolution ScalarSolution;

    template<typename TimeManager>
    ComplexEquiProblem(const TimeManager& timeManager, const GridView &gridView) :
    ParentType(gridView), eps_(1e-8)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
    }

    void init()
    {
        this->variables().initialize();
        for (int i = 0; i < this->gridView().size(0); i++)
        {
            this->variables().cellData(i).setSaturation(wPhaseIdx, 1.0);
        }
        this->model().initialize();
    }

    const char *name() const
    {
        return name_.c_str();
    }

    void addOutputVtkFields()
     {
         this->spatialParams().addOutputVtkFields(this->resultWriter());
     }

    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 273.15 + 10;
    }

    Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
    {
        return 1;
    }

    void source(PrimaryVariables &values,const Element& element) const
    {
        values = 0;
    }

    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
    {
        bcTypes.setAllDirichlet();
//         if (globalPos[0] > this->bBoxMax()[0] - eps_ ) // right
//         {
//             bcTypes.setAllNeumann();
//         }
//         else if (globalPos[0] < this->bBoxMin()[0] + eps_) // left
//         {
//             bcTypes.setAllNeumann();
//         }
    }

    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values[pwIdx] = 1.0 + 3.0*globalPos[1];
//         if (globalPos[1] > this->bBoxMax()[1] - eps_ )
//         {
//             values[pwIdx] = 4.0; // top
//         }
//         else
//             values[pwIdx] = 1.0; // bottom
    }

    void neumannAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values = 0;
    }

private:
    const Scalar eps_;
    std::string name_;
};

} // end namespace Dumux

#endif
