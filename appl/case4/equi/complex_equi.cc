// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#include "config.h"
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/version.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/io/file/dgfparser.hh>
#include <dumux/common/start.hh>

#include "complex_equi_problem.hh"

void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of mandatory options for this program is:\n"
                           "\t-TimeManager.TEnd              End of the simulation [s] \n"
                           "\t-TimeManager.DtInitial         Initial timestep size [s] \n"
                           "\t-Grid.File                     Name of the file containing the grid definition\n"
                           "\t-Problem.Name                  String for naming of the output files \n"
                           "\n";

        std::cout << errorMessageOut << std::endl;
    }
}

int main(int argc, char** argv)
{
    typedef TTAG(ComplexEquiProblem) TypeTag;
    return Dumux::start<TypeTag>(argc, argv, usage);
}
