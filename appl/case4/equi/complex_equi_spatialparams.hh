// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_COMPLEX_EQUI_SPATIALPARAMS_HH
#define DUMUX_COMPLEX_EQUI_SPATIALPARAMS_HH

#include <dumux/decoupled/common/decoupledproperties.hh>
#include <dumux/material/spatialparams/fvspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class ComplexEquiSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(ComplexEquiSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(ComplexEquiSpatialParams, SpatialParams, Dumux::ComplexEquiSpatialParams<TypeTag>);

// Set the material law
SET_PROP(ComplexEquiSpatialParams, MaterialLaw)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef LinearMaterial<Scalar> RawMaterialLaw;
public:
    typedef EffToAbsLaw<RawMaterialLaw> type;
};
}

template<class TypeTag>
class ComplexEquiSpatialParams: public FVSpatialParams<TypeTag>
{
    typedef FVSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> ElementMapper;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP(TypeTag, SolutionTypes)::ScalarSolution ScalarSolution;
    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    const FieldMatrix& intrinsicPermeability(const Element &element) const
    {
        if (GridCreator::getElementDomainMarker(element) == 0)
            return kMatrix_;
        else if (GridCreator::getElementDomainMarker(element) == 1)
            return kConductive_;
        else if (GridCreator::getElementDomainMarker(element) == 2)
            return kBlocking_;
        else if (GridCreator::getElementDomainMarker(element) == 3)
            return kCrossing_;
        else
            DUNE_THROW(Dune::NotImplemented, "permeability for element marker outside [0, 3]");
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }

    const MaterialLawParams& materialLawParams(const Element &element) const
    {
            return materialLawParams_;
    }

    template<class Writer>
    void addOutputVtkFields(Writer& writer)
    {
        ScalarSolution *perm = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *marker = writer.allocateManagedBuffer(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(; eIt != eEndIt; ++eIt)
        {
            int globalIdx = mapper_.map(*eIt);
            const auto& k = intrinsicPermeability(*eIt);
            (*perm)[globalIdx][0] = k[0][0];
            (*marker)[globalIdx][0] = GridCreator::getElementDomainMarker(*eIt);
        }

        writer.attachCellData(*perm, "permeability");
        writer.attachCellData(*marker, "element marker");
    }

    ComplexEquiSpatialParams(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), mapper_(gridView)
    , kMatrix_(0), kConductive_(0), kBlocking_(0), kCrossing_(0)
    {
        for (unsigned i = 0; i < dim; ++i)
        {
            kMatrix_[i][i] = 1.0;
            kConductive_[i][i] = 1e4;
            kBlocking_[i][i] = 1e-4;
            kCrossing_[i][i] = 1.9999999e-4; // harmonic mean of 1e4 and 1e-4
        }
    }

private:
    const GridView gridView_;
    MaterialLawParams materialLawParams_;
    ElementMapper mapper_;
    FieldMatrix kMatrix_;
    FieldMatrix kConductive_;
    FieldMatrix kBlocking_;
    FieldMatrix kCrossing_;
};

} // end namespace
#endif
