Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

B. Flemisch, I. Berre, W. Boon, A. Fumagalli, N. Schwenck, A. Scotti, I. Stefansson and A. Tatomir<br>
Benchmarks of single-phase flow in fractured porous media<br>
2016

Installation
============

The easiest way to install this module is to create a new folder and to execute
the file [installFlemisch2016a.sh]
(https://git.iws.uni-stuttgart.de/dumux-pub/Flemisch2016a/raw/master/installFlemisch2016a.sh)
in this folder.
You can copy the following to a terminal:
```bash
mkdir -p Flemisch2016a && cd Flemisch2016a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Flemisch2016a/raw/master/installFlemisch2016a.sh
chmod +x installFlemisch2016a.sh && ./installFlemisch2016a.sh
```
For more detailed informations on installation, have a look at the
[DuMuX installation guide](http://www.dumux.org/installation.php)
or use the [DuMuX handbook]
(http://www.dumux.org/documents/dumux-handbook-2.9.pdf), chapter 2.

Applications
============

The applications can be found in the folder `appl`, particularly in the
following subfolders. Each program is related to a corresponding section in the
PhD thesis.

* `decoupled`: programs for calculating reference solutions on equi-dimensional
  grids.
    - `complex`: Benchmark 4.
    - `geigerimp`: Benchmark 3.
    - `geigerper`: Benchmark 2.
    - `hydrocoin`: Benchmark 1.
* `dfm`: programs using the discrete fracture matrix (DFM) method.
    - `complex`: Benchmark 4.
    - `geiger`: Benchmarks 2 and 3.
    - `hydrocoin`: Benchmark 1.
* `xfem/comparison`: programs using the extended finite element method (XFEM).
    - `complex`: Benchmark 4.
    - `geiger`: Benchmarks 2 and 3.
    - `hydrocoin`: Benchmark 1.


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules as well as
grid managers, have a look at [installFlemisch2016a.sh]
(https://git.iws.uni-stuttgart.de/dumux-pub/Flemisch2016a/raw/master/installFlemisch2016a.sh).

In addition, the linear solver SuperLU has to be installed.

The module has been tested successfully with GCC 4.8.
The autotools-based DUNE buildsystem has been employed.
